-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2016 at 09:09 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `university`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
`id` int(11) NOT NULL,
  `stname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `fname` varchar(64) NOT NULL,
  `mname` varchar(64) NOT NULL,
  `date` date NOT NULL,
  `address` text NOT NULL,
  `gender` text NOT NULL,
  `contact` varchar(64) NOT NULL,
  `department` varchar(64) NOT NULL,
  `hobby` varchar(64) NOT NULL,
  `deleted_at` varchar(127) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `stname`, `email`, `fname`, `mname`, `date`, `address`, `gender`, `contact`, `department`, `hobby`, `deleted_at`) VALUES
(1, 'rahad', 'rahad@abc.com', 'Amirul', 'Daisy', '2016-01-14', 'mirpur', 'male', '+8801722435220', 'CSE', 'Photography,Cycling', NULL),
(2, 'Jaman', 'jaman@gmail.com', 'Rafiqul', 'Mehrun', '2016-01-14', 'Dhaka', 'male', '+8801670150414', 'CSE', 'Cycling,Programming', NULL),
(7, 'Suraiya', 'asdjhba@gmail.com', 'aliufba nds', 'auihfsnkam', '2016-01-01', 'ajkhgdsb ajn uahsndk', 'female', '5648641321', 'CSE', 'Photography,Cycling', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
