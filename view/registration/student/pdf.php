
<?php

ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\registration\student\Student;
use \App\registration\Utility\Utility;

$student = new Student();
$students = $student->index();
$trs = "";
?>
<?php


foreach ($students as $st):
   
    $trs .="<tr>";

    $trs .="<td>" . $st['id'] . "</td>";
    $trs .="<td>" . $st['stname'] . "</td>";
    $trs .="<td>" . $st['email'] . "</td>";
    $trs .="<td>" . $st['gender'] . "</td>";
    $trs .="<td>" . $st['contact'] . "</td>";
    $trs .="<td>" . $st['department'] . "</td>";


    $trs .="</tr>";
endforeach;
?>
<?php

$html = <<<mazhar



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phone Book</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <ins><h3 class="text-center">Student List</h3></ins> 


                <hr>

            
                <div class="row">


                    <table class="table table-bordered text-center bg-info">
                        <thead>
                            <tr>
                             
                                <th class="text-center">ID.</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Gender</th>
                                <th class="text-center">Contact</th>
                                <th class="text-center">Department</th>
                                

                            </tr>
                        </thead>
                        <tbody>

                     echo $trs;        
                        </tbody>
                    </table>
                </div>


        </section>

      
    </body>
</html>
mazhar;
?>
<?php

require_once './../../../vendor/mpdf/mpdf/mpdf.php';
$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
