<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Recover Book</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'studentreg' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

        use App\registration\student\Student;
        use \App\registration\Utility\Utility;

$ccc = new Student();
        $books = $ccc->trashed();
        ?>

        <div class="container">

            <h3 class="text-center text-success">Trashed Book List</h3>

            <div id="msg" style="background-color: #46b8da; color: #F00; font-size: 25px;">
                <?php echo Utility::message(); ?>            
            </div>
            <hr />
            <form action="recovermultiple.php" method="post">
                <button type="submit" class="btn btn-success ">Recover</button>
                <div><button type="button" id="deleteAll">Delete</button></div>
                <table class="table table-bordered table-hover text-center bg-info">
                    <thead >
                        <tr>
                            <th><input type="checkbox" name="markall" id="markall" ></th>
                            <th class="text-center">Sr No.</th>
                            <th class="text-center">ID</th>
                            <th class="text-center">Book name</th>
                            <th colspan="3" class="text-center">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($books) > 0) {
                            $sr = 1;
                            foreach ($books as $bk) {
                                ?>
                                <tr>
                                    <td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $bk->id; ?>"></td>
                                    <td><?php echo $sr ?></td>
                                    <td><?php echo $bk->id ?></td>
                                    <td><?php
                                        echo $bk->stname;
                                        $sr++;
                                        ?></td>
                                    <td><a href="recover.php?id=<?php echo $bk->id ?>" class="btn btn-warning ">Recover</a></td>
                                    <td>
                                        <a href="delete.php?id=<?php echo $bk->id ?>" class="btn btn-danger ">Delete</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td><p class="text-primary">No record available</p></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </form>

            <a href="" class="pull-left "><button class="btn btn-danger" onclick="window.history.go(-1)">Back</button></a>
            <a href="index.php"  class="pull-right "><button class="btn btn-success ">Back to Book List</button></a>

            <nav class="text-center">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
        <script>
                $(document).ready(function () {
                    $('.delete').bind('click', function (e) {
                        var dlt = confirm("Are you sure to Delete book Title");
                        if (!dlt) {
                            e.preventDefault();
                        }
                    });
                    $('#msg').fadeOut(5000);


                    $('#markall').bind('click', function () {
                        if ($('#markall').is(':checked')) {
                            $('.mark').each(function () { //loop through each checkbox
                                this.checked = true;  //select all checkboxes with class "mark"               
                            });
                        } else {
                            $('.mark').each(function () { //loop through each checkbox
                                this.checked = false;  //select all checkboxes with class "mark"               
                            });
                        }
                    });

                    $('#deleteAll').bind('click', function (e) {

                        var startDeleteProcess = false;
                        $('.mark').each(function () { //loop through each checkbox
                            if ($(this).is(':checked')) {
                                startDeleteProcess = true;
                            }
                        });

                        if (startDeleteProcess) {
                            var deleteItem = confirm("Are you sure you want to delete all Items??");
                            if (deleteItem) {
                                document.forms[0].action = 'deletemultiple.php';
                                document.forms[0].submit();
                            }
                        } else {
                            alert("Please select an item first");
                        }


                    });
                });
        </script>


    </body>
</html>