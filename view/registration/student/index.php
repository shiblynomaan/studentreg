<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'studentreg' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

use App\registration\student\Student;
use \App\registration\Utility\Utility;

$ccc = new Student();
$days = $ccc->index();

$filter = array();
$filterName = isset($_POST['filterName']) ? $_POST['filterName'] : "";
$filterContact = isset($_POST['filterContact']) ? $_POST['filterContact'] : "";


if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
    $filter = $_POST;
    $days = $ccc->index($filter);
}
//    $search = "";
//if (strtoupper($_SERVER['REQUEST_METHOD']) == 'GET') {
//    $search = isset($_GET['search']) ? $_GET : array('search' => '');
//    $days = $ccc->index($search);
//}
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Student List</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <section>
            <div class="container"> 
                <h2 class="p-v-lg m-t-xxl text-center text-danger">Student's List</h2>
                <hr />
                <div id="msg" style="background-color: #46b8da; color: #F00; font-size: 25px;">
                    <?php echo @Utility::message(); ?> 

                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <a href="create.php" class="btn btn-success ">Add new Student</a>
                    </div>
                    <div class="col-sm-2">
                        <a href="trashed.php" class="btn btn-success ">All trash</a>
                    </div>
                    <div class="col-sm-2 col-sm-offset-4">
                        <a href="pdf.php" class="btn btn-success ">Download as PDF</a>
                    </div>
                    <div class="col-sm-2">
                        <a href="xl.php" class="btn btn-success ">Download as XL</a>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-inline" action="index.php" method="post" >

                            <label>Filter By Name:</label>
                            <input class="form-control" type="text" name="filterName" value="<?php echo $filterName; ?>" /> 

                            <label>Filter By Phone No:</label>
                            <input class="form-control" type="text" name="filterContact" id="filterTitle" value="<?php echo $filterContact; ?>" /> 

                            <button class="btn-sm btn-danger" type="submit"> GO </button>

                        </form>
                    </div>
                    <div class="form-inline col-sm-4 ">
<!--                        <form action="index.php" method="get" >
                            <div class="">
                                <label>Search</label>
                                <input class="form-control" type="text" name="search" value="<?php echo $search['search']; ?>" /> 

                                <button class="btn-sm btn-success" type="submit"> Search </button>
                            </div>
                        </form>-->
                    </div>

                </div>

                <table class="table table-bordered table-hover text-center bg-info">
                    <thead >
                        <tr>
                            <th class="text-center">Serial</th>
                            <th class="text-center">ID</th>
                            <th class="text-center">Student name</th>
                            <th colspan="3" class="text-center">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $srl = 1;
                        if (count($days) > 0) {
                            foreach ($days as $day) {
                                ?>
                                <tr>
                                    <td><?php echo $srl ?></td>
                                    <td><?php echo $day['id'] ?></td>
                                    <td><?php echo $day['stname']; ?></td>
                                    <td><a href="show.php?id=<?php echo $day['id'] ?>" class="btn btn-success ">View</a></td>
                                    <td><a href="edit.php?id=<?php echo $day['id'] ?>" class="btn btn-primary ">Edit</a></td>
                                    <td><a href="trash.php?id=<?php echo $day['id'] ?>" class="btn btn-warning ">Trash</a></td>
                                    <td>
                                        <form action="delete.php" method="POST">
                                            <input type="hidden" name="id" value="<?php echo $day['id']; ?>"/>
                                            <button class="btn btn-danger delete" type="submit" >Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                <?php
                                $srl++;
                            }
                        } else {
                            ?>
                            <tr><td colspan="5"> No record found</td></tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../assets/js/bootstrap.min.js"></script>

        <script>
            $('.delete').bind('click', function (e) {
                var dlt = confirm("Are you sure to Delete Student info !");
                if (!dlt) {
                    e.preventDefault();
                }
            });
            $('#msg').fadeOut(5000);
        </script>

    </body>
</html>