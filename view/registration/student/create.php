<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Student Register | 2016</title>

        <!-- Bootstrap -->
        <link href="./../../../assets/css/bootstrap.css" rel="stylesheet">
        <link href="./../../../assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="./../../../assets/css/style.css" rel="stylesheet">
        <link href="./../../../assets/css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font awsome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <body class="bg-info">

        <section>
            <div class="container">

                <h2>Student registration form</h2>
                <form class="" role="form" action="store.php" method="POST">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="">
                                <img class="img-responsive" src="../../../assets/img/univ.png">
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="stname">Student name: </label>
                                    <input type="text" class="form-control" name="stname" placeholder="" />
                                </div>
                                <div class="col-sm-12">
                                    <label for="email">Email: </label>
                                    <input type="email" class="form-control" name="email" placeholder="" />
                                </div>
                                <div class="col-sm-12">
                                    <label for="fname">Father name: </label>
                                    <input type="text" class="form-control" name="fname" placeholder="" />
                                </div>
                                <div class="col-sm-12">
                                    <label for="mname">Mother name: </label>
                                    <input type="text" class="form-control" name="mname" placeholder="" />
                                </div>
                                <div class="col-sm-12">
                                    <label for="mname">Registration date: </label>
                                    <input type="date" class="form-control" name="date" placeholder="" />
                                </div>
                                <div class="col-sm-12">
                                    <label for="address">Address: </label>
                                    <input type="text" class="form-control" name="address" placeholder="" />
                                </div>
                                <div class="col-sm-6">
                                    <label for="gender">Gender </label>
                                    <select class="form-control" name="gender">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="contact">Phone no: </label>
                                    <input type="text" class="form-control" name="contact" placeholder="" />
                                </div>
                                <div class="col-sm-12 m-v-md form-group">
                                    <label for="department">Department </label><br>
                                    <input type="radio" name="department" value="EEE" />EEE
                                    <input type="radio" name="department" value="CSE" />CSE
                                    <input type="radio" name="department" value="CE" />CE
                                    <input type="radio" name="department" value="LAW" />LAW
                                    <input type="radio" name="department" value="ENGLISH" />ENGLISH
                                </div>
                                <div class="col-sm-12 p-v-md form-group">
                                    <label for="hobby">Hobbies </label><br>
                                    <input type="checkbox" name="hobby[]" value="Photography" />Photography
                                    <input type="checkbox" name="hobby[]" value="Cycling" />Cycling
                                    <input type="checkbox" name="hobby[]" value="Programming" />Programming
                                    <input type="checkbox" name="hobby[]" value="Debating" />Debating
                                    <input type="checkbox" name="hobby[]" value="Tour" />Tour
                                </div>
                                <div class="col-sm-12">
                                    
                                </div>

                            </div>
                            <button type="submit" class="btn btn-block btn-primary">Submit</button>
                        </div>

                    </div>

                </form>
        </section>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../assets/js/bootstrap.min.js"></script>


    </body>
</html>